module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: "iut Aria",
    meta: [
      {
        charset: "utf-8"
      },
      {
        name: "viewport",
        content: "width=device-width, initial-scale=1"
      },
      {
        hid: "description",
        name: "description",
        content: "IUT Aria"
      }
    ],
    link: [
      {
        rel: "icon",
        type: "image/x-icon",
        href: "/favicon.ico"
      },
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons"
      },
      {
        rel: "stylesheet",
        href:
          "https://cdn.rawgit.com/rastikerdar/vazir-font/v18.0.1/dist/font-face.css"
      }
    ]
  },

  /*
   ** Customize the progress bar color
   */
  loading: {
    color: "#3B8070"
  },

  /*
   ** Build configuration
   */
  build: {

    extractCSS: true,
    // babel: {
    //   presets: [
    //     ['vue-app'],  ["env", {
    //       "targets": {
    //         "browsers": [" > 1%", "safari >= 5"]
    //       }
    //     }]
    //   ]
    // },
    vendor: ["~/plugins/rg-setup.js", "~/plugins/vuetify.js", "axios", "vuelidate", "raven-js"]
  },
  plugins: ["~/plugins/rg-setup.js", "~/plugins/vuetify.js"],
  css: ["~/assets/style/app.styl"],

  /*
   ** Run ESLint on save
   */
  // extend(config, ctx) {
  //   if (ctx.isDev && ctx.isClient) {
  //     config.module.rules.push({
  //       enforce: "pre",
  //       test: /\.(js|vue)$/,
  //       loader: "eslint-loader",
  //       exclude: /(node_modules)/
  //     })
  //   }
  // }
  modules: [
    // Simple usage
    ["@nuxtjs/google-analytics", {
      id: "UA-119925874-1"
    }],
    "@nuxtjs/sentry"

  ],
  env: {
    DEV: process.env.DEV,
    API_URL: process.env.DEV ?
      "http://localhost:5000/api" :
      "https://iutgames.herokuapp.com/api"
  },
  // mode: "spa",
  sentry: {
    dsn: process.env.DEV ?
      "https://dfcd31021b834b49a7478ffaabcff676@sentry.io/1205321" :
      "https://b371b36ab3be4280b55565d6ac09b436@sentry.io/1214299"
  }
};
